package com.ppl.siakngnewbe.irsmahasiswa;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.ppl.siakngnewbe.kelasirs.KelasIrs;
import com.ppl.siakngnewbe.mahasiswa.Mahasiswa;
import com.ppl.siakngnewbe.tahunajaran.TahunAjaran;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity(name = "IrsMahasiswa")
@JsonIgnoreProperties(value = {"mahasiswa"})
public class IrsMahasiswa implements Serializable {

    @Id
    private String idIrs;

    @ManyToOne
    @JoinColumn(name = "mahasiswa")
    private Mahasiswa mahasiswa;

    @Column
    private int semester;

    @Enumerated(EnumType.STRING)
    private PersetujuanIRSStatus statusPersetujuan = PersetujuanIRSStatus.BELUM_DISETUJUI;

    @Column
    private boolean nilaiDefined = false;

    @Column
    private double totalMutu = 0.0;

    @Column
    private int sksa = 0;

    @Column
    private int sksl = 0;

    @OneToMany(mappedBy = "irs")
    @JsonManagedReference
    private Set<KelasIrs> kelasIrsSet;

    @ManyToOne
    @JoinColumn(name = "tahunAjaran")
    private TahunAjaran tahunAjaran;

    @Column
    private IrsMahasiswa prev;

    public boolean hasPrev() {
        return this.prev != null;
    }

    public double getIps() {
        return sksa == 0 ? 0 : totalMutu / sksa;
    }

    public void calculateTotalMutu() {
        this.totalMutu = 0.0;
        if (kelasIrsSet != null) {
            for (KelasIrs kelasIrs : kelasIrsSet) {
                this.totalMutu += kelasIrs.getBobot() * kelasIrs.getKelas().getSks();
            }
        }
    }

     public void calculateSksl() {
        this.sksl = 0;
        if (kelasIrsSet != null) {
            for (KelasIrs kelasIrs : kelasIrsSet) {
                if (kelasIrs.isLulus())
                    this.sksl += kelasIrs.getKelas().getSks();
            }
        }
    }
}

package com.ppl.siakngnewbe.penilaian;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KomponenPenilaianRepository extends JpaRepository<KomponenPenilaian, String> {
    KomponenPenilaian findById(Long id);
}

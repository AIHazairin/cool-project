package com.ppl.siakngnewbe.penilaian;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NilaiRepository extends JpaRepository<Nilai, String> {
    Nilai findById(Long id);
}

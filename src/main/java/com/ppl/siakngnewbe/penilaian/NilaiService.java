package com.ppl.siakngnewbe.penilaian;

import com.ppl.siakngnewbe.kelasirs.KelasIrsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NilaiService {

    @Autowired
    private NilaiRepository nilaiRepository;

    @Autowired
    private KelasIrsService kelasIrsService;

    public Nilai updateNilai(Long id, Double angka) {
        Nilai nilai = nilaiRepository.findById(id);
        nilai.setAngka(angka);
        nilaiRepository.save(nilai);
        kelasIrsService.calcEmptyAttribute(nilai.getKelasIrs().getId());
        return nilai;
    }
}

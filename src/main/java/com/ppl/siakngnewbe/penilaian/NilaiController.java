package com.ppl.siakngnewbe.penilaian;

import com.ppl.siakngnewbe.rest.BaseResponse;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/nilai")
@AllArgsConstructor
public class NilaiController {

    @Autowired
    private NilaiService nilaiService;

    @PostMapping(path = "/{id}/{angka}", produces = {"application/json"})
    @ResponseBody
    public BaseResponse<Object> updateNilai(@PathVariable(value = "id") long id,
                                            @PathVariable(value = "angka") double angka) {
        BaseResponse<Object> response = new BaseResponse<>();
        response.setStatus(200);
        response.setMessage("sukses");
        response.setResult(nilaiService.updateNilai(id, angka));
        return response;
    }
}

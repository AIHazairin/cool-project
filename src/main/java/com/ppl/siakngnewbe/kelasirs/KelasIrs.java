package com.ppl.siakngnewbe.kelasirs;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ppl.siakngnewbe.irsmahasiswa.IrsMahasiswa;
import com.ppl.siakngnewbe.kelas.Kelas;

import com.ppl.siakngnewbe.penilaian.Nilai;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "KelasIrs")
@JsonIgnoreProperties(value = {"id"})
public class KelasIrs implements Serializable {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private long id;

    @ManyToOne
    @JoinColumn(name = "irs")
    @JsonBackReference
    private IrsMahasiswa irs;

    @ManyToOne
    @JoinColumn(name = "kelas")
    private Kelas kelas;

    @Column
    private int posisi;

    @Column
    private double nilaiAkhir;

    @Column(columnDefinition = "varchar(255) default 'I'")
    private String nilaiHuruf;

    @Column
    private boolean lulus;

    @JsonIgnore
    @OneToMany(mappedBy = "kelasIrs")
    private List<Nilai> nilaiList;

    @Column
    private double bobot = 0.0;

    public void statusLulus() {
        this.lulus = this.bobot >= 2.00;
    }

    public void calculateBobot() {
        switch (Objects.requireNonNull(this.nilaiHuruf)) {
            case "A":
                this.bobot = 4.00;
                break;
            case "A-":
                this.bobot = 3.70;
                break;
            case "B+":
                this.bobot = 3.30;
                break;
            case "B":
                this.bobot = 3.00;
                break;
            case "B-":
                this.bobot = 2.70;
                break;
            case "C+":
                this.bobot = 2.30;
                break;
            case "C":
                this.bobot = 2.00;
                break;
            case "D":
                this.bobot = 1.00;
                break;
            default:
                this.bobot = 0.0;
        }
    }

    public void calculateNilaiAkhir() {
        this.nilaiAkhir = 0.0;
        for (Nilai nilai : nilaiList) {
            this.nilaiAkhir += nilai.getAngka() * nilai.getKomponenPenilaian().getBobot();
        }
    }
}

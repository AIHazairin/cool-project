package com.ppl.siakngnewbe.kelasirs;

import com.ppl.siakngnewbe.irsmahasiswa.IrsMahasiswa;
import com.ppl.siakngnewbe.irsmahasiswa.IrsMahasiswaService;
import com.ppl.siakngnewbe.penilaian.KomponenPenilaian;
import com.ppl.siakngnewbe.penilaian.KomponenPenilaianRepository;
import com.ppl.siakngnewbe.penilaian.Nilai;
import com.ppl.siakngnewbe.penilaian.NilaiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KelasIrsService {

    @Autowired
    private KelasIrsRepository kelasIrsRepository;

    @Autowired
    private IrsMahasiswaService irsMahasiswaService;

    public void calcEmptyAttribute(long id) {
        KelasIrs kelasIrs = kelasIrsRepository.findById(id);
        kelasIrs.calculateNilaiAkhir();

        if (kelasIrs.getNilaiAkhir() >= 85) {
            kelasIrs.setNilaiHuruf("A");
        } else if (kelasIrs.getNilaiAkhir() >= 80) {
            kelasIrs.setNilaiHuruf("A-");
        } else if (kelasIrs.getNilaiAkhir() >= 75) {
            kelasIrs.setNilaiHuruf("B+");
        } else if (kelasIrs.getNilaiAkhir() >= 70) {
            kelasIrs.setNilaiHuruf("B");
        } else if (kelasIrs.getNilaiAkhir() >= 65) {
            kelasIrs.setNilaiHuruf("B-");
        } else if (kelasIrs.getNilaiAkhir() >= 60) {
            kelasIrs.setNilaiHuruf("C+");
        } else if (kelasIrs.getNilaiAkhir() >= 55) {
            kelasIrs.setNilaiHuruf("C");
        } else if (kelasIrs.getNilaiAkhir() >= 40) {
            kelasIrs.setNilaiHuruf("D");
        } else {
            kelasIrs.setNilaiHuruf("E");
        }

        kelasIrs.calculateBobot();
        kelasIrs.statusLulus();
        kelasIrsRepository.save(kelasIrs);
        irsMahasiswaService.calcEmptyAttribute(kelasIrs.getIrs().getIdIrs());
    }
}
